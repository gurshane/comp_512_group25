package ResInterface;

import java.io.Serializable;
import java.rmi.RemoteException;

public class MethodResponseImpl implements MethodResponse, Serializable{
    int intResult;
    boolean boolResult;
    String stringResult;
    boolean isDone;

    public MethodResponseImpl(int intResult, boolean boolResult, String stringResult)
    {
        this.intResult = intResult;
        this.boolResult = boolResult;
        this.stringResult = stringResult;
        this.isDone = false;
    }

    @Override
    public int getIntResult() throws RemoteException {
        return this.intResult;
    }

    @Override
    public boolean getBoolResult() throws RemoteException
    {
        return this.boolResult;
    }

    @Override
    public String getStringResult() throws RemoteException
    {
        return this.stringResult;
    }

    @Override
    public void setIntResult (int result)  throws RemoteException { this.intResult = result; }

    @Override
    public void setBoolResult (boolean boolResult)  throws RemoteException { this.boolResult = boolResult;}

    @Override
    public void setStringResult(String stringResult)  throws RemoteException { this.stringResult = stringResult;}

    @Override
    public boolean done()  throws RemoteException { return this.isDone;}

    @Override
    public void doneWorking()  throws RemoteException { this.isDone = true;}

    @Override
    public void notDoneWorking() throws RemoteException { this.isDone = false;}
}
