package ResInterface;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

//Data structure that holds the response of a completed method call
public interface MethodResponse extends Remote
{

    public int getIntResult() throws RemoteException;

    public boolean getBoolResult() throws  RemoteException;

    public String getStringResult() throws RemoteException;

    public void setIntResult (int result) throws RemoteException;

    public void setBoolResult (boolean boolResult)  throws RemoteException;

    public void setStringResult(String stringResult) throws RemoteException;

    public boolean done() throws RemoteException;

    public void doneWorking() throws RemoteException;

    public void notDoneWorking () throws RemoteException;

}
