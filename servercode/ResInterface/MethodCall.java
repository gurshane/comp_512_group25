package ResInterface;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.HashMap;

public class MethodCall implements Serializable {

    public enum RMTYPE
    {
        CAR,
        ROOM,
        FLIGHT,
        ALL
    }

    private String methodName;
    private int jobId;
    private int transactionId;
    private HashMap<String, String> arguments;
    private RMTYPE rmtype;
    private MethodResponse methodResponse;

    public MethodCall(String methodName, HashMap arguments) {
        this.methodName = methodName;
        this.arguments = arguments;
    }

    public String getMethodName()
    {
        return this.methodName;
    }

    public int getJobId() {return jobId;}

    public void setJobId(int inId) { this.jobId = inId;}

    public RMTYPE getRmtype() {
        return this.rmtype;
    }

    public void setRmtype(RMTYPE rmtype) {
        this.rmtype = rmtype;
    }

    public int getTransactionId()
    {
        return this.transactionId;
    }

    public void setTransactionId(int id)
    {
        this.transactionId = id;
    }

    public HashMap<String, String> getArguments()
    {
        return this.arguments;
    }

    public void setArguments(HashMap<String, String> given) {this.arguments = given;}

    public void setMethodReponse(MethodResponse mr)
    {
        this.methodResponse = mr;
    }

    public MethodResponse getMethodReponse()
    {
        return this.methodResponse;
    }
}