// -------------------------------
// adapted from Kevin T. Manley
// CSE 593
//
package ResImpl;

import ResInterface.*;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;

import javax.xml.crypto.Data;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.*;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RMISecurityManager;

/*
TODO:
   -> commit()
   -> addOperation()

*/




//ResourceManager being a manager of a Car, Flight, or Room resource
public class ResourceManagerImpl extends ReceiverAdapter implements ResourceManager
{
    protected HashMap<Integer, RMHashtable> m_transactionCaches = new HashMap<>();
    protected RMHashtable m_itemHT = new RMHashtable();

    protected HashMap<Integer, HashMap<String, RMItem>> m_transactionCache = new HashMap<>();
    protected HashMap<Integer, List<MethodCall>> m_transactionMethodCalls = new HashMap<>();
    protected HashMap<Integer, Boolean> m_abortStatus = new HashMap<>();
    protected LockManager m_LockManager = new LockManager();

    JChannel rmChannel;

    public static void main(String args[]) {

        // Figure out where server is running
        String server = "";
        int port = 1099;
        String id = "";

        if (args.length == 3) {
            server = args[0];
            port = Integer.parseInt(args[1]);
            id = "Group25" + args[2];
        } else if (args.length != 0 &&  args.length != 1) {
            System.err.println ("Wrong usage");
            System.out.println("Usage: java ResImpl.ResourceManagerImpl [registryHost] [registryPort] [rmID]");
            System.exit(1);
        }

        try {
            // create a new Server object
            ResourceManagerImpl obj = new ResourceManagerImpl(args[2]);

           /*// dynamically generate the stub (client proxy)
           ResourceManager rm = (ResourceManager) UnicastRemoteObject.exportObject(obj, 0);

           // Bind the remote object's stub in the registry
           Registry registry = LocateRegistry.getRegistry(server, port);

           //add this resource manager to the registry
           registry.rebind(id, rm);*/



            System.err.println("RM ready");
        } catch (Exception e) {
            System.err.println("RM exception: " + e.toString());
            e.printStackTrace();
        }

        // Create and install a security manager
        if (System.getSecurityManager() == null) {
            //noinspection deprecation
            System.setSecurityManager(new RMISecurityManager());
        }
    }

    public ResourceManagerImpl(String rmName) throws RemoteException
    {
        try {
            this.rmChannel = new JChannel();
            this.rmChannel.setReceiver(this);
            this.rmChannel.connect(rmName);
//            System.out.println("Getting previous state...");
//            this.rmChannel.getState(null, 10000);
//            System.out.println("State received.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receive(Message msg) {

        try {
            Integer i = (Integer) msg.getObject();
            this.m_LockManager.UnlockAll(i);
            return;
        } catch (Exception e) {

        }

        //if this was a state update, update the state and get out
        try {
            DataRMState dataRMState = (DataRMState) msg.getObject();

            if(dataRMState != null)
            {
                this.m_abortStatus = dataRMState.m_abortStatus;
                this.m_LockManager = dataRMState.m_LockManager;
                this.m_transactionCache = dataRMState.m_transactionCache;
                this.m_transactionCaches = dataRMState.m_transactionCaches;
                this.m_itemHT = dataRMState.m_itemHT;
                this.m_transactionMethodCalls = dataRMState.m_transactionMethodCalls;
                return;
            }

        } catch (Exception e)
        {
        }

        MethodCall methodCall = (MethodCall)msg.getObject();
        System.out.println("***********************RM Name got a message: " + this.rmChannel.getName() + " Method Name: " + methodCall.getMethodName());
        MethodResponse mrStub = methodCall.getMethodReponse();

        String rmName = "";

        switch(methodCall.getRmtype()) {
            case CAR:
                rmName = "Car";
                break;
            case ROOM:
                rmName = "Room";
                break;
            case FLIGHT:
                rmName = "Flight";
                break;
            default:
                rmName = "All";
        }

        System.out.println("************ RM NOde is going to do work *****************");


        try {
            addOperation(methodCall.getTransactionId(), methodCall, rmName);
        } catch (InvalidTransactionException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }



        try {
            while(!mrStub.done());
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        System.out.println("*************** RM NODE is done doing work : " + mrStub +  " **********************" );
    }

    // Reads a data item
    private RMItem readData( int id, String key)
    {
        RMHashtable dataStore;
       /*
           A transaction ID of -1 means you operate on the core database.
           Otherwise, operate on the cache that corresponds to that transaction.
        */
        if (id != -1) {

            dataStore = this.m_transactionCaches.get(id);
            if (dataStore == null) {
                dataStore = new RMHashtable(m_itemHT);
                this.m_transactionCaches.put(id, dataStore);
            }
        } else {
            dataStore = this.m_itemHT;
        }
        synchronized(dataStore) {
            return (RMItem) dataStore.get(key);
        }
    }

    // Writes a data item
    private void writeData( int id, String key, RMItem value)
    {
        RMHashtable dataStore;

        if (id != -1) {
            dataStore = this.m_transactionCaches.get(id);
            if (dataStore == null) {
                dataStore = new RMHashtable(m_itemHT);
                this.m_transactionCaches.put(id, dataStore);
            }
        } else {
            dataStore = this.m_itemHT;
        }
        synchronized(dataStore) {
            dataStore.put(key, value);
        }

        updateEveryone();
    }

    private void updateEveryone()
    {
        Message m = new Message(null, new DataRMState(this.m_transactionCaches, this.m_itemHT, this.m_transactionCache, this.m_transactionMethodCalls,
                this.m_abortStatus, this.m_LockManager));
        try {
            this.rmChannel.send(m);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Remove the item out of storage
    protected RMItem removeData(int id, String key) {

        RMHashtable dataStore;

        if (id != -1) {
            dataStore = this.m_transactionCaches.get(id);
            if (dataStore == null) {
                dataStore = new RMHashtable(m_itemHT);
                this.m_transactionCaches.put(id, dataStore);
            }
        } else {
            dataStore = this.m_itemHT;
        }
        synchronized(dataStore) {
            return (RMItem)dataStore.remove(key);
        }
    }


    // deletes the entire item
    protected boolean deleteItem(int id, String key)
    {
        Trace.info("RM::deleteItem(" + id + ", " + key + ") called" );
        ReservableItem curObj = (ReservableItem) readData( id, key);
        // Check if there is such an item in the storage
        if ( curObj == null ) {
            Trace.warn("RM::deleteItem(" + id + ", " + key + ") failed--item doesn't exist" );
            return false;
        } else {
            if (curObj.getReserved()==0) {
                removeData(id, curObj.getKey());
                Trace.info("RM::deleteItem(" + id + ", " + key + ") item deleted" );
                return true;
            }
            else {
                Trace.info("RM::deleteItem(" + id + ", " + key + ") item can't be deleted because some customers reserved it" );
                return false;
            }
        } // if
    }


    // query the number of available seats/rooms/cars
    protected int queryNum(int id, String key) {
        Trace.info("RM::queryNum(" + id + ", " + key + ") called" );
        ReservableItem curObj = (ReservableItem) readData( id, key);
        int value = 0;
        if ( curObj != null ) {
            value = curObj.getCount();
        } // else
        Trace.info("RM::queryNum(" + id + ", " + key + ") returns count=" + value);
        return value;
    }

    // query the price of an item
    protected int queryPrice(int id, String key) {
        Trace.info("RM::queryCarsPrice(" + id + ", " + key + ") called" );
        ReservableItem curObj = (ReservableItem) readData( id, key);
        int value = 0;
        if ( curObj != null ) {
            value = curObj.getPrice();
        } // else
        Trace.info("RM::queryCarsPrice(" + id + ", " + key + ") returns cost=$" + value );
        return value;
    }

    // reserve an item
    protected boolean reserveItem(int id, int customerID, String key, String location) {
        Trace.info("RM::reserveItem( " + id + ", customer=" + customerID + ", " +key+ ", "+location+" ) called" );
        // Read customer object if it exists (and read lock it)
        Customer cust = (Customer) readData( id, Customer.getKey(customerID));
        if ( cust == null ) {
            Trace.warn("RM::reserveCar( " + id + ", " + customerID + ", " + key + ", "+location+")  failed--customer doesn't exist" );
            return false;
        }

        // check if the item is available
        ReservableItem item = (ReservableItem)readData(id, key);
        if ( item == null ) {
            Trace.warn("RM::reserveItem( " + id + ", " + customerID + ", " + key+", " +location+") failed--item doesn't exist" );
            return false;
        } else if (item.getCount()==0) {
            Trace.warn("RM::reserveItem( " + id + ", " + customerID + ", " + key+", " + location+") failed--No more items" );
            return false;
        } else {
            //Move this to the middleware. It should do this iff this method returns true
            cust.reserve( key, location, item.getPrice());
            writeData( id, cust.getKey(), cust);

            // decrease the number of available items in the storage
            item.setCount(item.getCount() - 1);
            item.setReserved(item.getReserved()+1);

            Trace.info("RM::reserveItem( " + id + ", " + customerID + ", " + key + ", " +location+") succeeded" );
            return true;
        }
    }

    // Create a new flight, or add seats to existing flight
    //  NOTE: if flightPrice <= 0 and the flight already exists, it maintains its current price
    public boolean addFlight(int id, int flightNum, int flightSeats, int flightPrice)
            throws RemoteException
    {
        Trace.info("RM::addFlight(" + id + ", " + flightNum + ", $" + flightPrice + ", " + flightSeats + ") called" );
        Flight curObj = (Flight) readData( id, Flight.getKey(flightNum));
        if ( curObj == null ) {
            // doesn't exist...add it
            Flight newObj = new Flight( flightNum, flightSeats, flightPrice );
            writeData( id, newObj.getKey(), newObj);
            Trace.info("RM::addFlight(" + id + ") created new flight " + flightNum + ", seats=" +
                    flightSeats + ", price=$" + flightPrice );
        } else {
            // add seats to existing flight and update the price...
            curObj.setCount( curObj.getCount() + flightSeats );
            if ( flightPrice > 0 ) {
                curObj.setPrice( flightPrice );
            } // if
            writeData( id, curObj.getKey(), curObj);
            Trace.info("RM::addFlight(" + id + ") modified existing flight " + flightNum + ", seats=" + curObj.getCount() + ", price=$" + flightPrice );
        } // else
        return(true);
    }



    public boolean deleteFlight(int id, int flightNum)
            throws RemoteException
    {
        return deleteItem(id, Flight.getKey(flightNum));
    }



    // Create a new room location or add rooms to an existing location
    //  NOTE: if price <= 0 and the room location already exists, it maintains its current price
    public boolean addRooms(int id, String location, int count, int price)
            throws RemoteException
    {
        Trace.info("RM::addRooms(" + id + ", " + location + ", " + count + ", $" + price + ") called" );
        Hotel curObj = (Hotel) readData( id, Hotel.getKey(location) );
        if ( curObj == null ) {
            // doesn't exist...add it
            Hotel newObj = new Hotel( location, count, price );
            writeData( id, newObj.getKey(), newObj );
            Trace.info("RM::addRooms(" + id + ") created new room location " + location + ", count=" + count + ", price=$" + price );
        } else {
            // add count to existing object and update price...
            curObj.setCount( curObj.getCount() + count );
            if ( price > 0 ) {
                curObj.setPrice( price );
            } // if
            writeData( id, curObj.getKey(), curObj );
            Trace.info("RM::addRooms(" + id + ") modified existing location " + location + ", count=" + curObj.getCount() + ", price=$" + price );
        } // else
        return(true);
    }

    // Delete rooms from a location
    public boolean deleteRooms(int id, String location)
            throws RemoteException
    {
        return deleteItem(id, Hotel.getKey(location));

    }

    // Create a new car location or add cars to an existing location
    //  NOTE: if price <= 0 and the location already exists, it maintains its current price
    public boolean addCars(int id, String location, int count, int price)
            throws RemoteException
    {
        Trace.info("RM::addCars(" + id + ", " + location + ", " + count + ", $" + price + ") called" );
        Car curObj = (Car) readData( id, Car.getKey(location) );
        if ( curObj == null ) {
            // car location doesn't exist...add it
            Car newObj = new Car( location, count, price );
            writeData( id, newObj.getKey(), newObj );
            Trace.info("RM::addCars(" + id + ") created new location " + location + ", count=" + count + ", price=$" + price );
        } else {
            // add count to existing car location and update price...
            curObj.setCount( curObj.getCount() + count );
            if ( price > 0 ) {
                curObj.setPrice( price );
            } // if
            writeData( id, curObj.getKey(), curObj );
            Trace.info("RM::addCars(" + id + ") modified existing location " + location + ", count=" + curObj.getCount() + ", price=$" + price );
        } // else
        return(true);
    }


    // Delete cars from a location
    public boolean deleteCars(int id, String location)
            throws RemoteException
    {
        return deleteItem(id, Car.getKey(location));
    }



    // Returns the number of empty seats on this flight
    public int queryFlight(int id, int flightNum)
            throws RemoteException
    {
        return queryNum(id, Flight.getKey(flightNum));
    }

    // Returns the number of reservations for this flight.
//    public int queryFlightReservations(int id, int flightNum)
//        throws RemoteException
//    {
//        Trace.info("RM::queryFlightReservations(" + id + ", #" + flightNum + ") called" );
//        RMInteger numReservations = (RMInteger) readData( id, Flight.getNumReservationsKey(flightNum) );
//        if ( numReservations == null ) {
//            numReservations = new RMInteger(0);
//        } // if
//        Trace.info("RM::queryFlightReservations(" + id + ", #" + flightNum + ") returns " + numReservations );
//        return numReservations.getValue();
//    }


    // Returns price of this flight
    public int queryFlightPrice(int id, int flightNum )
            throws RemoteException
    {
        return queryPrice(id, Flight.getKey(flightNum));
    }


    // Returns the number of rooms available at a location
    public int queryRooms(int id, String location)
            throws RemoteException
    {
        return queryNum(id, Hotel.getKey(location));
    }




    // Returns room price at this location
    public int queryRoomsPrice(int id, String location)
            throws RemoteException
    {
        return queryPrice(id, Hotel.getKey(location));
    }


    // Returns the number of cars available at a location
    public int queryCars(int id, String location)
            throws RemoteException
    {
        return queryNum(id, Car.getKey(location));
    }


    // Returns price of cars at this location
    public int queryCarsPrice(int id, String location)
            throws RemoteException
    {
        return queryPrice(id, Car.getKey(location));
    }

    // Returns data structure containing customer reservation info. Returns null if the
    //  customer doesn't exist. Returns empty RMHashtable if customer exists but has no
    //  reservations.
    public RMHashtable getCustomerReservations(int id, int customerID)
            throws RemoteException
    {
        Trace.info("RM::getCustomerReservations(" + id + ", " + customerID + ") called" );
        Customer cust = (Customer) readData( id, Customer.getKey(customerID));
        if ( cust == null ) {
            Trace.warn("RM::getCustomerReservations failed(" + id + ", " + customerID + ") failed--customer doesn't exist" );
            return null;
        } else {
            return cust.getReservations();
        } // if
    }

    // return a bill
    public String queryCustomerInfo(int id, int customerID)
            throws RemoteException
    {
        Trace.info("RM::queryCustomerInfo(" + id + ", " + customerID + ") called" );
        Customer cust = (Customer) readData( id, Customer.getKey(customerID));
        if ( cust == null ) {
            Trace.warn("RM::queryCustomerInfo(" + id + ", " + customerID + ") failed--customer doesn't exist" );
            return "";   // NOTE: don't change this--WC counts on this value indicating a customer does not exist...
        } else {
            String s = cust.printBill();
            Trace.info("RM::queryCustomerInfo(" + id + ", " + customerID + "), bill follows..." );
            System.out.println( s );
            return s;
        } // if
    }

    // customer functions
    // new customer just returns a unique customer identifier

    public int newCustomer(int id)
            throws RemoteException
    {
        Trace.info("INFO: RM::newCustomer(" + id + ") called" );
        // Generate a globally unique ID for the new customer
        int cid = Integer.parseInt( String.valueOf(id) +
                String.valueOf(Calendar.getInstance().get(Calendar.MILLISECOND)) +
                String.valueOf( Math.round( Math.random() * 100 + 1 )));
        Customer cust = new Customer( cid );
        writeData( id, cust.getKey(), cust );
        Trace.info("RM::newCustomer(" + cid + ") returns ID=" + cid );
        return cid;
    }

    // I opted to pass in customerID instead. This makes testing easier
    public boolean newCustomer(int id, int customerID)
            throws RemoteException
    {
        Trace.info("INFO: RM::newCustomer(" + id + ", " + customerID + ") called" );
        Customer cust = (Customer) readData( id, Customer.getKey(customerID) );
        if ( cust == null ) {
            cust = new Customer(customerID);
            writeData( id, cust.getKey(), cust );
            Trace.info("INFO: RM::newCustomer(" + id + ", " + customerID + ") created a new customer" );
            return true;
        } else {
            Trace.info("INFO: RM::newCustomer(" + id + ", " + customerID + ") failed--customer already exists");
            return false;
        } // else
    }


    // Deletes customer from the database.
    public boolean deleteCustomer(int id, int customerID)
            throws RemoteException
    {
        Trace.info("RM::deleteCustomer(" + id + ", " + customerID + ") called" );
        Customer cust = (Customer) readData( id, Customer.getKey(customerID) );
        if ( cust == null ) {
            Trace.warn("RM::deleteCustomer(" + id + ", " + customerID + ") failed--customer doesn't exist" );
            return false;
        } else {
            // Increase the reserved numbers of all reservable items which the customer reserved.
            RMHashtable reservationHT = cust.getReservations();
            for (Enumeration e = reservationHT.keys(); e.hasMoreElements();) {
                String reservedkey = (String) (e.nextElement());
                ReservedItem reserveditem = cust.getReservedItem(reservedkey);
                Trace.info("RM::deleteCustomer(" + id + ", " + customerID + ") has reserved " + reserveditem.getKey() + " " +  reserveditem.getCount() +  " times"  );
                ReservableItem item  = (ReservableItem) readData(id, reserveditem.getKey());
                Trace.info("RM::deleteCustomer(" + id + ", " + customerID + ") has reserved " + reserveditem.getKey() + "which is reserved" +  item.getReserved() +  " times and is still available " + item.getCount() + " times"  );
                item.setReserved(item.getReserved()-reserveditem.getCount());
                item.setCount(item.getCount()+reserveditem.getCount());
            }

            // remove the customer from the storage
            removeData(id, cust.getKey());

            Trace.info("RM::deleteCustomer(" + id + ", " + customerID + ") succeeded" );
            return true;
        } // if
    }



   /*
   // Frees flight reservation record. Flight reservation records help us make sure we
   // don't delete a flight if one or more customers are holding reservations
   public boolean freeFlightReservation(int id, int flightNum)
       throws RemoteException
   {
       Trace.info("RM::freeFlightReservations(" + id + ", " + flightNum + ") called" );
       RMInteger numReservations = (RMInteger) readData( id, Flight.getNumReservationsKey(flightNum) );
       if ( numReservations != null ) {
           numReservations = new RMInteger( Math.max( 0, numReservations.getValue()-1) );
       } // if
       writeData(id, Flight.getNumReservationsKey(flightNum), numReservations );
       Trace.info("RM::freeFlightReservations(" + id + ", " + flightNum + ") succeeded, this flight now has "
               + numReservations + " reservations" );
       return true;
   }
   */


    // Adds car reservation to this customer.
    public boolean reserveCar(int id, int customerID, String location)
            throws RemoteException
    {
        return reserveItem(id, customerID, Car.getKey(location), location);
    }


    // Adds room reservation to this customer.
    public boolean reserveRoom(int id, int customerID, String location)
            throws RemoteException
    {
        return reserveItem(id, customerID, Hotel.getKey(location), location);
    }
    // Adds flight reservation to this customer.
    public boolean reserveFlight(int id, int customerID, int flightNum)
            throws RemoteException
    {
        return reserveItem(id, customerID, Flight.getKey(flightNum), String.valueOf(flightNum));
    }

    // Reserve an itinerary
    public boolean itinerary(int id,int customer,Vector flightNumbers,String location,boolean Car,boolean Room)
            throws RemoteException
    {
        return false;
    }

   /*
       TRANSACTION RELATED METHODS
    */

    @Override
    public int start() throws RemoteException
    {
        //select a unique transaction Id for this user
        System.out.println("at rm");
        int transId = (new Random()).nextInt(1000);
        Integer transInt = new Integer(transId);

        boolean conflict = m_transactionCache.containsKey(transInt);

        while(conflict)
        {
            transId = (new Random()).nextInt(1000);
            transInt = new Integer(transId);
            conflict = m_transactionCache.containsKey(transInt);
        }

        System.out.println("rm id: " + transId);

        m_transactionCache.put(transInt, new HashMap<>());
        m_transactionMethodCalls.put(transInt, new ArrayList<MethodCall>());
        m_abortStatus.put(transInt, false);

        return transId;
    }

    @Override
    public boolean commit(int transactionId) throws RemoteException, TransactionAbortedException, InvalidTransactionException
    {
        //now we can go through and perform each operation on this RM for this transaction
        List<MethodCall> methodCalls = m_transactionMethodCalls.get(new Integer(transactionId));
        if(methodCalls != null && methodCalls.size() > 0)
        {
            for(MethodCall methodCall : m_transactionMethodCalls.get(new Integer(transactionId)))
            {
                //keeps track of the result of each method call by name
                try {
                    executeOperation(transactionId, methodCall, true);
                } catch (DeadlockException e) {
//                e.printStackTrace();
                    System.out.println("Deadlock timeout trying to get resource. Try again later.");
                }
            }

            m_transactionMethodCalls.remove(transactionId);
            m_LockManager.UnlockAll(transactionId);
            unlockLocks(transactionId);
        }

        //always returns true because we assume that if a transaction gets this far, it is good to commit
        return true;
    }

    public void unlockLocks(int transactionId)
    {
        Message m = new Message(null, new Integer(transactionId));
        try {
            this.rmChannel.send(m);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //executes an operation on this rm
    //TODO: Next step is to add x-lock and s-lock aquisition to each case and to add a boolean that will determine if this rm should actually do a certain operation
    //      or just acquire the x-lock and return
    public MethodResponse executeOperation(int transactionId, MethodCall methodCall, boolean atCommitTime) throws RemoteException, DeadlockException {
        HashMap<String, String> clientArgs = methodCall.getArguments();
        String methodName = methodCall.getMethodName();

        boolean boolResult = true;
        int intResult = Integer.MAX_VALUE;
        String stringResult = "";

        MethodResponse methodResponse;

        Date currentDate = new Date();
        long startTime = currentDate.getTime();

        if (atCommitTime) {
            transactionId = -1;
        }

        System.out.println("****************executing operation****************");

        switch (methodName) {
            case ("startTrans"):
                intResult = start();
                break;
            case ("commitTrans"):
                try {
                    boolResult = commit(transactionId);
                } catch (TransactionAbortedException e) {
                    e.printStackTrace();
                } catch (InvalidTransactionException e) {
                    e.printStackTrace();
                }
                break;
            case ("abortTrans"):
                try {
                    abort(transactionId);
                } catch (InvalidTransactionException e) {
                    e.printStackTrace();
                }
                break;
            case ("addFlight"):
                if(!atCommitTime)
                {
                    m_LockManager.Lock(transactionId, "Flight:" + clientArgs.get("flightNum"), LockManager.WRITE);
                }
                boolResult = addFlight(
                        transactionId,
                        Integer.parseInt(clientArgs.get("flightNum")),
                        Integer.parseInt(clientArgs.get("flightSeats")),
                        Integer.parseInt(clientArgs.get("flightPrice")));
                break;

            case ("addCars"):
                if(!atCommitTime)
                {
                    m_LockManager.Lock(transactionId, "Cars:" + clientArgs.get("location"), LockManager.WRITE);
                }
                boolResult = addCars(
                        transactionId,
                        clientArgs.get("location"),
                        Integer.parseInt(clientArgs.get("numCars")),
                        Integer.parseInt(clientArgs.get("price")));
                break;

            case ("addRooms"):
                if(!atCommitTime)
                {
                    m_LockManager.Lock(transactionId, "Rooms:" + clientArgs.get("location"), LockManager.WRITE);
                }
                boolResult = addRooms(
                        transactionId,
                        clientArgs.get("location"),
                        Integer.parseInt(clientArgs.get("numRooms")),
                        Integer.parseInt(clientArgs.get("price")));
                break;

            case ("newCustomer"):
                if(!atCommitTime)
                {
                    m_LockManager.Lock(transactionId, "Customer:" + clientArgs.get("customerID"), LockManager.WRITE);
                }
                boolResult = newCustomer(
                        transactionId,
                        Integer.parseInt(clientArgs.get("customerID")));

                break;

            case ("deleteFlight"):
                if(!atCommitTime)
                {
                    m_LockManager.Lock(transactionId, "Flight:" + clientArgs.get("flightNum"), LockManager.WRITE);
                }
                boolResult = deleteFlight(
                        transactionId,
                        Integer.parseInt(clientArgs.get("flightNum")));
                break;

            case ("deleteCars"):
                if(!atCommitTime)
                {
                    m_LockManager.Lock(transactionId, "Cars:" + clientArgs.get("location"), LockManager.WRITE);
                }
                boolResult = deleteCars(
                        transactionId,
                        clientArgs.get("location"));
                break;

            case ("deleteRooms"):
                if(!atCommitTime)
                {
                    m_LockManager.Lock(transactionId, "Rooms:" + clientArgs.get("location"), LockManager.WRITE);
                }
                boolResult = deleteRooms(
                        transactionId,
                        clientArgs.get("location"));
                break;

            case ("deleteCustomer"):
                if(!atCommitTime)
                {
                    m_LockManager.Lock(transactionId, "Customer:" + clientArgs.get("customer"), LockManager.WRITE);
                }
                System.out.println(clientArgs.get("customer"));
                boolResult = deleteCustomer(
                        transactionId,
                        Integer.parseInt(clientArgs.get("customer")));
                break;

            case ("queryFlight"):
                if(!atCommitTime)
                {
                    //try and get an s-lock
                    m_LockManager.Lock(transactionId, "Flight:" + clientArgs.get("flightNum"), LockManager.READ);
                }
                intResult = queryFlight(
                        transactionId,
                        Integer.parseInt(clientArgs.get("flightNum")));
                break;

            case ("queryCars"):
                if(!atCommitTime)
                {
                    //try and get an s-lock
                    m_LockManager.Lock(transactionId, "Cars:" + clientArgs.get("location"), LockManager.READ);
                }
                intResult = queryCars(
                        transactionId,
                        clientArgs.get("location"));
                break;

            case ("queryRooms"):
                if(!atCommitTime)
                {
                    //try and get an s-lock
                    m_LockManager.Lock(transactionId, "Rooms:" + clientArgs.get("location"), LockManager.READ);
                }
                intResult = queryRooms(
                        transactionId,
                        clientArgs.get("location"));
                break;

            case ("queryCustomerInfo"):
                if(!atCommitTime)
                {
                    //try and get an s-lock
                    m_LockManager.Lock(transactionId, "Customer:" + clientArgs.get("customer"), LockManager.READ);
                }
                stringResult = queryCustomerInfo(
                        transactionId,
                        Integer.parseInt(clientArgs.get("customer")));
                break;

            case ("queryFlightPrice"):
                if(!atCommitTime)
                {
                    //try and get an s-lock
                    m_LockManager.Lock(transactionId, "Flight:" + clientArgs.get("flightNum"), LockManager.READ);
                }
                intResult = queryFlightPrice(
                        transactionId,
                        Integer.parseInt(clientArgs.get("flightNum")));
                break;

            case ("queryCarsPrice"):
                if(!atCommitTime)
                {
                    //try and get an s-lock
                    m_LockManager.Lock(transactionId, "Cars:" + clientArgs.get("location"), LockManager.READ);
                }
                intResult = queryCarsPrice(
                        transactionId,
                        clientArgs.get("location"));
                break;

            case ("queryRoomsPrice"):
                if(!atCommitTime)
                {
                    //try and get an s-lock
                    m_LockManager.Lock(transactionId, "Rooms:" + clientArgs.get("location"), LockManager.READ);
                }
                intResult = queryRoomsPrice(
                        transactionId,
                        clientArgs.get("location"));
                break;

            case ("reserveFlight"):
                if(!atCommitTime)
                {
                    //try and get an x-lock
                    m_LockManager.Lock(transactionId, "Flight:" + clientArgs.get("flightNumber"), LockManager.WRITE);
                }
                boolResult = reserveFlight(
                        transactionId,
                        Integer.parseInt(clientArgs.get("customer")),
                        Integer.parseInt(clientArgs.get("flightNumber")));
                break;

            case ("reserveCar"):
                if(!atCommitTime)
                {
                    //try and get an x-lock
                    m_LockManager.Lock(transactionId, "Cars:" + clientArgs.get("location"), LockManager.WRITE);
                }
                boolResult = reserveCar(
                        transactionId,
                        Integer.parseInt(clientArgs.get("customer")),
                        clientArgs.get("location"));
                break;

            case ("reserveRoom"):
                if(!atCommitTime)
                {
                    //try and get an x-lock
                    m_LockManager.Lock(transactionId, "Rooms:" + clientArgs.get("location"), LockManager.WRITE);
                }
                boolResult = reserveRoom(
                        transactionId,
                        Integer.parseInt(clientArgs.get("customer")),
                        clientArgs.get("location"));
                break;

            case ("itinerary"):
                if(!atCommitTime)
                {
                    //try and get an x-lock
                    //TODO:Figure out lock situation here
                    break;
                }
                Vector<Integer> flightNumbers = new Vector<Integer>();
                String vectorString = clientArgs.get("flightNumbers");

                vectorString = vectorString.replaceAll("[\\[ \\]]", "");

                System.out.println("vectorString: " + vectorString);

                String[] vecItems = vectorString.split(",");
                for (String item : vecItems) {
                    flightNumbers.add(Integer.valueOf(item));
                }
                boolResult = itinerary(
                        transactionId,
                        Integer.parseInt(clientArgs.get("customer")),
                        flightNumbers,
                        clientArgs.get("location"),
                        Boolean.valueOf(clientArgs.get("Car")),
                        Boolean.valueOf(clientArgs.get("Room")));
                break;

            default:
                break;
        }

        if(atCommitTime)
        {
            double elapsedTime = (new Date().getTime() - startTime)/1000000.0;
            System.out.println("Total elapsed time for : " + methodCall.getMethodName() + " : at rm is = " + elapsedTime);
        }

        System.out.println("****************operation executed****************");

        System.out.println("Result for " + methodCall.getMethodName() + " : intResult: " + intResult + " boolResult: " + boolResult + " stringResult: " + stringResult);

        return new MethodResponseImpl(intResult, boolResult, stringResult);
    }

    @Override
    public void abort(int transactionId) throws RemoteException, InvalidTransactionException
    {
        Integer transInt = new Integer(transactionId);

        m_transactionCache.remove(transInt);
        m_transactionMethodCalls.remove(transInt);
        m_abortStatus.remove(transInt);
        m_LockManager.UnlockAll(transactionId);

        unlockLocks(transactionId);
    }

    @Override
    public boolean shutdown() throws RemoteException
    {
        return false;
    }

    @Override
    public boolean getAbortStatus(int transactionId) throws RemoteException
    {
        Integer transInt = new Integer(transactionId);

        //if this transaction exists locally and has been asked to abort, then return true
        if(m_abortStatus.containsKey(transInt) && m_abortStatus.get(transInt))
        {
            return true;
        }

        return false;
    }

    @Override
    public String getChannelName() throws RemoteException {
        return "";
    }

    @Override
    public MethodResponse addOperation(int transactionId, MethodCall methodCall, String name) throws InvalidTransactionException, RemoteException
    {
        MethodResponse methodResponse;

        Date currentDate = new Date();

        try
        {
            //if this isnt a read operation, add it to the write set
            if(!methodCall.getMethodName().contains("query") && !methodCall.getMethodName().contains("abort") && !methodCall.getMethodName().contains("commit") && !methodCall.getMethodName().contains("start"))
            {
                System.out.println("These are all the write sets stored on this machine: " + m_transactionMethodCalls.toString());

                System.out.println("Getting the write set for transaction: " + transactionId);

                List<MethodCall> currentOperations = m_transactionMethodCalls.get(new Integer(transactionId));
                if(currentOperations == null)
                {
                    m_transactionCache.put(transactionId, new HashMap<>());
                    m_transactionMethodCalls.put(transactionId, new ArrayList<MethodCall>());
                    m_abortStatus.put(transactionId, false);
                }

                currentOperations= m_transactionMethodCalls.get(new Integer(transactionId));
                System.out.println("storing transaction: " + currentOperations.size());
                currentOperations.add(methodCall);
                System.out.println("updating transactionList");
                m_transactionMethodCalls.put(new Integer(transactionId), currentOperations);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("************************about to execute transaction");
        //if this is a read operation then an s-lock will be grabbed o/w an x-lock will be
        try
        {
            methodResponse = executeOperation(transactionId, methodCall, false);
            MethodResponse mrStub = methodCall.getMethodReponse();

            mrStub.setBoolResult(methodResponse.getBoolResult());
            mrStub.setIntResult(methodResponse.getIntResult());
            mrStub.setStringResult(methodResponse.getStringResult());
            mrStub.doneWorking();

            updateEveryone();

            return methodResponse;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Deadlock timeout trying to get resource. Please try again later.");
        }


        return null;
    }
}


