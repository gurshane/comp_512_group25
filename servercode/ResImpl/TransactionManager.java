package ResImpl;

import ResInterface.*;
import org.jgroups.util.ExpiryCache;

import java.rmi.RemoteException;
import java.util.*;


public class TransactionManager {

    protected static LockManager lockManager;
    protected static ResourceManager carManager;
    protected static ResourceManager roomManager;
    protected static ResourceManager flightManager;

    protected HashMap<String, Integer> rmTransactionIds = new HashMap<>();
    protected HashSet<Integer> transIds = new HashSet<>();

    public TransactionManager(ResourceManager carManager, ResourceManager roomManager, ResourceManager flightManager)
    {
        this.lockManager = new LockManager();

        this.carManager = carManager;
        this.roomManager = roomManager;
        this.flightManager = flightManager;
    }

    public MiddlewareState getState() {
        return new MiddlewareState(this.rmTransactionIds, this.transIds);
    }

    public void setState(MiddlewareState state) {
        HashMap<String, Integer> rmTransactionIds = state.getRmTransactionIds();
        HashSet<Integer> transIds = state.getTransIds();

        this.rmTransactionIds.putAll(rmTransactionIds);
        this.transIds.addAll(transIds);
    }

    public int start() throws RemoteException {
        System.out.println("at transMan");
        //select a transaction Id for this client at the middleware
        int transId = (new Random()).nextInt(1000);
        Integer transInt = new Integer(transId);

        boolean conflict = transIds.contains(transInt);

        while(conflict)
        {
            transId = (new Random()).nextInt(1000);
            transInt = new Integer(transId);
            conflict = transIds.contains(transInt);
        }

        System.out.println("have id: " + transId);

        return transId;
    }

    public TrxnObj getTransaction(int id)
    {
        return null;
    }

    public synchronized boolean commit(int transactionId) throws RemoteException, TransactionAbortedException, InvalidTransactionException
    {

        //get the appropriate transId for each RM
        int carTransId = rmTransactionIds.get("Car-"+transactionId);
        int roomTransId = rmTransactionIds.get("Room-"+transactionId);
        int flightTransId = rmTransactionIds.get("Flight-"+transactionId);

        //verify that this transaction hasnt been asked to abort on any of the rms
        boolean carAbort = carManager.getAbortStatus(carTransId);
        boolean roomAbort = roomManager.getAbortStatus(roomTransId);
        boolean flightAbort = flightManager.getAbortStatus(flightTransId);

        //if at least one rm wants us to abort, we abort everywhere
        if(carAbort || roomAbort || flightAbort)
        {
            abort(transactionId);
            return false;
        }

        //no need to abort, commit everywhere
        carManager.commit(carTransId);
        roomManager.commit(roomTransId);
        flightManager.commit(flightTransId);

        return true;
    }

    public synchronized void abort(int transactionId) throws InvalidTransactionException, RemoteException {

        int carTransId = rmTransactionIds.get("Car-"+transactionId);
        int roomTransId = rmTransactionIds.get("Room-"+transactionId);
        int flightTransId = rmTransactionIds.get("Flight-"+transactionId);

        //abort this transaction at each rm
        carManager.abort(carTransId);
        roomManager.abort(roomTransId);
        flightManager.abort(flightTransId);
    }

    //sends the operation to the right rm
    //TODO convert transId to rmTransId
    public MethodResponse addOperation(int transactionId, MethodCall methodCall, String rmName) throws InvalidTransactionException, RemoteException {

        System.out.println("Adding an operation at the middleware node.");
        System.out.println("These are the current active operations: " + rmTransactionIds.toString());

        int carTransId = 0;
        int roomTransId = 0;
        int flightTransId = 0;

        try
        {
            carTransId = rmTransactionIds.get("Car-"+transactionId);
            roomTransId = rmTransactionIds.get("Room-"+transactionId);
            flightTransId = rmTransactionIds.get("Flight-"+transactionId);
            System.out.println("carID: " + carTransId + " roomId: " + roomTransId + " flightId " + flightTransId);
        }
        catch(Exception e)
        {

        }

        MethodResponse mrStub = methodCall.getMethodReponse();

        System.out.println("*********************STARTING METHOD: " + mrStub + " *******************");

        Date currentDate = new Date();
        long startTime = currentDate.getTime();

        if(rmName.equals("Car"))
        {
            carManager.addOperation(carTransId, methodCall, "Car");
            while(!mrStub.done()){}
        }
        else if(rmName.equals("Room"))
        {
            roomManager.addOperation(roomTransId, methodCall, "Room");
            while(!mrStub.done()){}
        }
        else if(rmName.equals("Flight"))
        {
            flightManager.addOperation(flightTransId, methodCall, "Flight");
            while(!mrStub.done()){}
        }
        else
        {
            //We have been asked to do an operation that involves all rms
            carManager.addOperation(carTransId, methodCall, "Car");
            roomManager.addOperation(roomTransId, methodCall, "Room");
            flightManager.addOperation(flightTransId, methodCall, "Flight");

            while(!mrStub.done()){}
        }

        System.out.println("***********DONE METHOD: " + mrStub + " ******************");

        double elapsedTime = (new Date().getTime()-startTime)/1000000.0;
        System.out.println("Total elapsed time for : " + methodCall.getMethodName() + " : at middleware/tm is = " + elapsedTime);

        return mrStub;
    }
}