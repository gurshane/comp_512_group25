package ResImpl;
import ResInterface.*;
import org.jgroups.*;
import java.io.*;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import static ResInterface.MethodCall.RMTYPE;
import static ResInterface.MethodCall.RMTYPE.*;


public class ClusterDaemon extends ReceiverAdapter implements ResourceManager
{


    JChannel jChannel;
    String channelName;
    static int jobCounter;

    ClusterState state;

    public ClusterDaemon(String channelName) throws Exception
    {
        this.jChannel = new JChannel();
        this.jChannel.setReceiver(this);
        this.jChannel.connect(channelName);
        this.channelName = channelName;
        this.jobCounter = 0;
        if (channelName.equals("middleware")) {
            this.state = new MiddlewareState();
        } else {
            this.state = new DataRMState();
        }
    }


    @Override
    public void viewAccepted(View new_view)
    {
        System.out.println("Cluster Daemon for " + this.channelName + " noticed activity  on the cluster");
    }


    @Override
    public void receive(Message msg)
    {
        try{
            DataRMState dataRMState = (DataRMState) msg.getObject();
            if(dataRMState != null){
                this.state = dataRMState;
            }
            return;
        }
        catch (Exception e)
        {
            System.out.println("Couldn't cast message to DataRMState");
        }

        try {
            MiddlewareState middlewareState = (MiddlewareState) msg.getObject();
            if (middlewareState != null) {
                this.state = middlewareState;
            }
        } catch (Exception e) {
            System.out.println("Couldn't cast message to MiddlewareState");
        }

        //TODO: Update state whenever a request to update broadcast is received from the Middleware Nodes.
        // Could use setState here, but we already send it.
//        ClusterState newState = msg.getObject();
//        System.out.println("Updating state...");
//        synchronized (this.state) {
//            this.state = newState;
//        }
//        System.out.println("State updated...");
    }


    public void send(MethodCall methodCall, boolean multicast)
    {
        methodCall.setJobId(this.jobCounter++);

        View currentView = this.jChannel.getView();
        List<Address> addresses = currentView.getMembers();

        MethodResponse methodResponse = new MethodResponseImpl(0, false, "");
        MethodResponse mrStub = methodCall.getMethodReponse();
        try
        {
            if(mrStub == null)
            {
                mrStub = (MethodResponse) UnicastRemoteObject.exportObject(methodResponse, 0);
                methodCall.setMethodReponse(mrStub);
                LocateRegistry.getRegistry(1099).rebind(mrStub.toString() + methodCall.getJobId(), mrStub);
                LocateRegistry.getRegistry(1099).rebind(methodCall.toString() + methodCall.getJobId(), methodResponse);
            }
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }

        Message message = null;
        if(multicast)
        {
            message = new Message(null, methodCall);
        }
        else
        {
            System.out.println("Number of people in: " + this.channelName + " : " + addresses.size());
            int addressToSendTo = Math.max(methodCall.getJobId() % addresses.size(), 1);
            message = new Message(addresses.get(addressToSendTo), methodCall);
        }

        try
        {
            this.jChannel.send(message);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("******************* WAITING FOR METHODRESPONSE ******************************");
        try {
            while(!mrStub.done()){}
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        System.out.println("***********************DONE: " + mrStub + " DONE****************************");
        //also return methodResponse
        //every method below needs to get methodReponse from send and extract relevant value for returnig

    }


    //TODO: how to get result back from method call execution
    //should listen for completion of JOBID + METHODCALL NAME
    @Override
    public boolean addFlight(int id, int flightNum, int flightSeats, int flightPrice) throws RemoteException
    {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("flightNum", Integer.toString(flightNum));
        methodArguments.put("flightSeats", Integer.toString(flightSeats));
        methodArguments.put("flightPrice", Integer.toString(flightPrice));

        MethodCall methodCall = new MethodCall("addFlight", methodArguments);
        methodCall.setRmtype(FLIGHT);
        methodCall.setTransactionId(id);

        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public boolean addCars(int id, String location, int numCars, int price) throws RemoteException
    {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);
        methodArguments.put("numCars", Integer.toString(numCars));
        methodArguments.put("price", Integer.toString(price));

        MethodCall methodCall = new MethodCall("addCars", methodArguments);
        methodCall.setRmtype(CAR);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public boolean addRooms(int id, String location, int numRooms, int price) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);
        methodArguments.put("numRooms", Integer.toString(numRooms));
        methodArguments.put("price", Integer.toString(price));

        MethodCall methodCall = new MethodCall("addRooms", methodArguments);
        methodCall.setRmtype(ROOM);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public int newCustomer(int id) throws RemoteException {

        System.out.println("Creating cid.");
        int cid = Integer.parseInt( String.valueOf(id) +
                String.valueOf(Calendar.getInstance().get(Calendar.MILLISECOND)) +
                String.valueOf( Math.round( Math.random() * 100 + 1 )));

        newCustomer(id, cid);
        return cid;
    }


    //TODO: This
    @Override
    public boolean newCustomer(int id, int cid) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customerID", Integer.toString(cid));

        MethodCall methodCall = new MethodCall("newCustomer", methodArguments);
        methodCall.setRmtype(ALL);
        methodCall.setTransactionId(id);
        send(methodCall, false);
        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public boolean deleteFlight(int id, int flightNum) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("flightNum", Integer.toString(flightNum));

        MethodCall methodCall = new MethodCall("deleteFlight", methodArguments);
        methodCall.setRmtype(FLIGHT);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public boolean deleteCars(int id, String location) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("deleteCars", methodArguments);
        methodCall.setRmtype(CAR);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public boolean deleteRooms(int id, String location) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("deleteRooms", methodArguments);
        methodCall.setRmtype(ROOM);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public boolean deleteCustomer(int id, int customer) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customer));

        MethodCall methodCall = new MethodCall("deleteCustomer", methodArguments);
        methodCall.setRmtype(ALL);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public int queryFlight(int id, int flightNumber) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("flightNum", Integer.toString(flightNumber));

        MethodCall methodCall = new MethodCall("queryFlight", methodArguments);
        methodCall.setRmtype(FLIGHT);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getIntResult();
    }

    @Override
    public int queryCars(int id, String location) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("queryCars", methodArguments);
        methodCall.setRmtype(CAR);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getIntResult();
    }

    @Override
    public int queryRooms(int id, String location) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("queryRooms", methodArguments);
        methodCall.setRmtype(ROOM);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getIntResult();
    }

    @Override
    public String queryCustomerInfo(int id, int customer) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customer));

        MethodCall methodCall = new MethodCall("queryCustomerInfo", methodArguments);
        methodCall.setRmtype(ALL);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getStringResult();
    }

    @Override
    public int queryFlightPrice(int id, int flightNumber) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("flightNum", Integer.toString(flightNumber));

        MethodCall methodCall = new MethodCall("queryFlightPrice", methodArguments);
        methodCall.setRmtype(FLIGHT);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getIntResult();
    }

    @Override
    public int queryCarsPrice(int id, String location) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("queryCarsPrice", methodArguments);
        methodCall.setRmtype(CAR);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getIntResult();
    }

    @Override
    public int queryRoomsPrice(int id, String location) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("queryRoomsPrice", methodArguments);
        methodCall.setRmtype(ROOM);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getIntResult();
    }

    @Override
    public boolean reserveFlight(int id, int customer, int flightNumber) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customer));
        methodArguments.put("flightNumber", Integer.toString(flightNumber));

        MethodCall methodCall = new MethodCall("reserveFlight", methodArguments);
        methodCall.setRmtype(FLIGHT);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public boolean reserveCar(int id, int customer, String location) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customer));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("reserveCar", methodArguments);
        methodCall.setRmtype(CAR);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public boolean reserveRoom(int id, int customer, String locationd) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customer));
        methodArguments.put("location", locationd);

        MethodCall methodCall = new MethodCall("reserveRoom", methodArguments);
        methodCall.setRmtype(ROOM);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public boolean itinerary(int id, int customer, Vector flightNumbers, String location, boolean Car, boolean Room) throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();
        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customer));
        methodArguments.put("flightNumbers", flightNumbers.toString());
        methodArguments.put("location", location);
        methodArguments.put("Car", Boolean.toString(Car));
        methodArguments.put("Room", Boolean.toString(Room));

        MethodCall methodCall = new MethodCall("itinerary", methodArguments);
        methodCall.setRmtype(ALL);
        methodCall.setTransactionId(id);
        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public int start() throws RemoteException
    {
        MethodCall methodCall = new MethodCall("startTrans", null);
        methodCall.setRmtype(RMTYPE.ALL);
        send(methodCall, false);
        return methodCall.getMethodReponse().getIntResult();
    }

    //TODO: pre-pend job id to method call name ex: "23:startTrans" or "1412:addFlight"
    @Override
    public boolean commit(int transactionId) throws RemoteException, TransactionAbortedException, InvalidTransactionException {
        HashMap<String, String> methodArguments = new HashMap<>();
        methodArguments.put("transId", Integer.toString(transactionId));

        MethodCall methodCall = new MethodCall("commitTrans", methodArguments);
        methodCall.setRmtype(ALL);
        methodCall.setTransactionId(transactionId);

        send(methodCall, false);

        return methodCall.getMethodReponse().getBoolResult();
    }

    @Override
    public void abort(int transactionId) throws RemoteException, InvalidTransactionException {
        HashMap<String, String> methodArguments = new HashMap<>();
        methodArguments.put("transId", Integer.toString(transactionId));

        MethodCall methodCall = new MethodCall("abortTrans", methodArguments);
        methodCall.setRmtype(ALL);
        methodCall.setTransactionId(transactionId);

        send(methodCall, false);
    }

    @Override
    public boolean shutdown() throws RemoteException {

        MethodCall methodCall = new MethodCall("shutdown", null);
        methodCall.setRmtype(ALL);

        send(methodCall, false);

        return false;
    }

    @Override
    public MethodResponse addOperation(int transactionId, MethodCall methodCall, String rmName) throws InvalidTransactionException, RemoteException {
        System.out.println("********************** cluster daemon is adding operation : " + this.channelName + " received : " + methodCall.getMethodName());
        methodCall.setTransactionId(transactionId);
        send(methodCall, false);
        return new MethodResponseImpl(0, false, "");
    }

    @Override
    public boolean getAbortStatus(int transactionId) throws RemoteException {
        return false;
    }

    @Override
    public String getChannelName() throws RemoteException {
        return this.channelName;
    }

    public static void main(String[] args) {

        String channelName = "";
        int registryPort = 0;

        if (args.length == 2) {
            channelName = args[0];
            registryPort = Integer.parseInt(args[1]);
        } else {
            System.out.println("USAGE: [channelName] [registryPort]");
        }
        try {
            System.out.println("Creating a new cluster daemon container object.");
            Registry myRegistry = LocateRegistry.getRegistry(registryPort);

            ClusterDaemon clusterDaemonObj = new ClusterDaemon(channelName);

            ResourceManager clusterDaemon = (ResourceManager) UnicastRemoteObject.exportObject(clusterDaemonObj, 0);

            myRegistry.rebind(channelName, clusterDaemon);

            System.out.println("Cluster container bound in the registry");
            System.out.println("Cluster daemon ready.");

        } catch(Exception e1) {
            e1.printStackTrace();
        }
    }
}
