package ResImpl;

import java.util.HashMap;
import java.util.HashSet;

public class MiddlewareState extends ClusterState {

    protected HashMap<String, Integer> rmTransactionIds = new HashMap<>();
    protected HashSet<Integer> transIds = new HashSet<>();

    public MiddlewareState(HashMap<String, Integer> rmTransactionIds, HashSet<Integer> transIds) {
        super();
        this.rmTransactionIds = rmTransactionIds;
        this.transIds = transIds;
    }

    public MiddlewareState() {
        super();
    }

    public HashSet<Integer> getTransIds() {
        return transIds;
    }

    public void setTransIds(HashSet<Integer> transIds) {
        this.transIds = transIds;
    }

    public HashMap<String, Integer> getRmTransactionIds() {
        return rmTransactionIds;
    }

    public void setRmTransactionIds(HashMap<String, Integer> rmTransactionIds) {
        this.rmTransactionIds = rmTransactionIds;
    }
}
