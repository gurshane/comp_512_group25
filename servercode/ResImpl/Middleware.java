package ResImpl;

import ResInterface.*;
import ResInterface.InvalidTransactionException;
import org.jgroups.*;

import java.io.*;
import java.util.*;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.RMISecurityManager;

/*
TODO:
   -> Need to finish up sending method calls to transaction manager
   -> Need to make use of the methodresponse that is returned from each rm which now contains the data that the middleware
       usually returned. See addFlight for example
*/

public class Middleware extends ReceiverAdapter implements ResourceManager {

//    @Override
//    public void setState(InputStream input) throws Exception {
//        System.out.println("Setting state on this middleware.");
//
//        View view = this.channel.getView();
//
//        // If there are more than two members, get the state from the coordinator.
//
//        if (view.getMembers().size() > 2) {
//            MiddlewareState state = (MiddlewareState) new ObjectInputStream(input).readObject();
//            this.transactionManager.setState(state);
//        }
//
//    }

    @Override
    public MethodResponse addOperation(int transactionId, MethodCall methodCall, String rmName) throws InvalidTransactionException, RemoteException {
        return null;
    }

    @Override
    public boolean getAbortStatus(int transactionId) throws RemoteException {
        return false;
    }

    protected static ResourceManager carManager;
    protected static ResourceManager roomManager;
    protected static ResourceManager flightManager;

    protected static TransactionManager transactionManager;

    protected static ResourceManager middlewareContainer;

    private String channelName;

    private JChannel channel;

    public static void main(String[] args) {

        int port = 1099;
        String carId = "";
        String flightId = "";
        String roomId = "";

        String carRMRHostName = "";
        int carRMRPort = 1099;

        String flightRMRHostName = "";
        int flightRMRPort = 1099;

        String roomRMRHostName = "";
        int roomRMRPort = 1099;

        String[] carInfo = null;
        String[] flightInfo = null;
        String[] roomInfo = null;

        Registry registry;

        // Add customer functionality to Middleware Implementation.
        //->Move customer code from ResourceManagerImpl.java to Here

        // Implement itinerary in the Middleware

        if (args.length == 4) {
            port = Integer.parseInt(args[0]);

            carInfo = args[1].split(":");
            flightInfo = args[2].split(":");
            roomInfo = args[3].split(":");

            carRMRHostName = carInfo[0];
            carRMRPort = Integer.parseInt(carInfo[1]);
            carId = carInfo[2];

            flightRMRHostName = flightInfo[0];
            flightRMRPort = Integer.parseInt(flightInfo[1]);
            flightId = flightInfo[2];

            roomRMRHostName = roomInfo[0];
            roomRMRPort = Integer.parseInt(roomInfo[1]);
            roomId = roomInfo[2];

        } else if (args.length != 0 &&  args.length != 1) {
            System.err.println ("Wrong usage");
            System.out.println("Usage: java ResImpl.Middleware [registryPort] carRMRHostName:carRMRPort:Group25Car Group25Flight Group25Room");
            System.exit(1);
        }

        try {

            // Bind the remote object's stub in the registry
            //no need for host name here as the middleware rm and registry run on the same machine
            registry = LocateRegistry.getRegistry(port);

            Registry carRegistry = LocateRegistry.getRegistry(carRMRHostName, carRMRPort);
            Registry flightRegistry = LocateRegistry.getRegistry(flightRMRHostName, flightRMRPort);
            Registry roomRegistry = LocateRegistry.getRegistry(roomRMRHostName, roomRMRPort);

            //Get CarRM
            carManager = (ResourceManager) carRegistry.lookup(carId);

            //Get FlightRM
            flightManager = (ResourceManager) flightRegistry.lookup(flightId);

            //Get RoomRM
            roomManager = (ResourceManager) roomRegistry.lookup(roomId);

            transactionManager = new TransactionManager(carManager, roomManager, flightManager);

        } catch (Exception e) {
            System.err.println("Middleware exception: " + e.toString());
            e.printStackTrace();
        }

        try {

            Middleware middlewareObj = new Middleware(carManager, flightManager, roomManager, transactionManager, "middleware");

            System.err.println("Middleware ready.");
        }

        catch(Exception e) {
            e.printStackTrace();
        }

        // Create and install a security manager
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
    }

    public Middleware(ResourceManager carManager, ResourceManager flightManager, ResourceManager roomManager, TransactionManager transactionManager, String channelName) throws Exception
    {
        this.flightManager = flightManager;
        this.carManager = carManager;
        this.roomManager = roomManager;

        this.transactionManager = transactionManager;

        this.channelName = channelName;

        JChannel channel = new JChannel().setReceiver(this);
        channel.connect(this.channelName);
        this.channel = channel;
//        System.out.println("Getting previous state...");
//        channel.getState(null, 10000); // Get the state when we just join the cluster.
//        System.out.println("Previous state received.");
    }

    @Override
    public String getChannelName() {
        return this.channelName;
    }

    private void updateEveryone()
    {
        Message m = new Message(null, this.transactionManager.getState());
        try {
            this.channel.send(m);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receive(Message msg) {

        try {
            MiddlewareState state = msg.getObject();
            if(state != null)
            {
                this.transactionManager.setState(state);

            }
            return;

        } catch (Exception e)
        {
            System.out.println("Couldn't cast message to MiddlewareState, must be a method call.");
        }

        System.out.println("Message source: " + msg.getSrc());

        MethodCall methodCall = null;

        methodCall = (MethodCall) msg.getObject();

        if(methodCall != null)
        {
            System.out.println("Middleware node received: " + methodCall.getMethodName());
            if(methodCall.getMethodReponse() != null)
            {
                System.out.println("and the stub is here too*****************");
            }
        }
        MethodResponse mrStub;

        String rmName = "";

        switch(methodCall.getRmtype())
        {
            case CAR:
                rmName = "Car";
                break;
            case ROOM:
                rmName = "Room";
                break;
            case FLIGHT:
                rmName = "Flight";
                break;
            default:
                rmName = "All";
                break;
        }
        System.out.println("*****************abouit to do work on MWN *****************");
        try
        {
            mrStub = methodCall.getMethodReponse();

            if(methodCall.getMethodName().equals("startTrans"))
            {
                System.out.println("*****************starting transaction*****************");
                int transId = this.transactionManager.start();
                System.out.println("*****************transId*****************" + transId);
                mrStub.setIntResult(transId);
                mrStub.doneWorking();
                mrStub.notDoneWorking();

                this.transactionManager.addOperation(transId, methodCall, rmName);
                this.transactionManager.rmTransactionIds.put("Car-"+transId, mrStub.getIntResult());
                mrStub.notDoneWorking();

                this.transactionManager.addOperation(transId, methodCall, rmName);
                mrStub.notDoneWorking();
                this.transactionManager.rmTransactionIds.put("Room-"+transId, mrStub.getIntResult());

                this.transactionManager.addOperation(transId, methodCall, rmName);
                this.transactionManager.rmTransactionIds.put("Flight-"+transId, mrStub.getIntResult());

                this.updateEveryone();
            }
            else
            {
                System.out.println("*****************MWN performing oepration*****************");
                this.transactionManager.addOperation(methodCall.getTransactionId(), methodCall, rmName);
                System.out.println("***********MiddlwareNode Wwaitng for methodREsponse ***************");
                while(!mrStub.done()) {}
                System.out.println("***************** middlware node done waiting" + mrStub + "**********");
            }


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        // Get the latest state, add our updates to it and send it back to the coordinator.
//
//        try {
//            this.channel.getState(null, 10000);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        View view = this.channel.getView();
//        Address coordinatorAddress = view.getCoord();
//
//        try {
//            this.channel.send(coordinatorAddress, this.transactionManager.getState());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        //determine which rm to send it to
//        if (msg.getSrc() == null) {
//
//        }
//        System.out.println("Received Message: ");
//        System.out.println("From: " + msg.getSrc());
//
//        MethodCall methodCall = msg.getObject();
//
//        System.out.println("This middleware received command " + methodCall.getMethodName());

//       View currentView = this.channel.getView();

      /* if message is from the container

          Perform appropriate operation e.g.call appropriate method on appropriate dataRMContainer or on transactionManager
          Create a message object with return value.
          Broadcast message (so others can update state.)

        if message is from another Middleware, update state.
       */
    }

    /* Add seats to a flight.  In general this will be used to create a new
             * flight, but it should be possible to add seats to an existing flight.
             * Adding to an existing flight should overwrite the current price of the
             * available seats.
             *
             * @return success.
             */
    public boolean addFlight(int id, int flightNum, int flightSeats, int flightPrice)
            throws RemoteException {

        // Add methodCall to WriteSet for this transaction.

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("flightNum", Integer.toString(flightNum));
        methodArguments.put("flightSeats", Integer.toString(flightSeats));
        methodArguments.put("flightPrice", Integer.toString(flightPrice));

        MethodCall methodCall = new MethodCall("addFlight", methodArguments);
        MethodResponse methodResponse;

        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Flight");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();
    }

    /* Add cars to a location.
     * This should look a lot like addFlight, only keyed on a string location
     * instead of a flight number.
     */
    public boolean addCars(int id, String location, int numCars, int price)
            throws RemoteException {

        // Add methodCall to WriteSet for this transaction.

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);
        methodArguments.put("numCars", Integer.toString(numCars));
        methodArguments.put("price", Integer.toString(price));

        MethodCall methodCall = new MethodCall("addCars", methodArguments);
        MethodResponse methodResponse;

        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Car");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();

//        return this.carManager.addCars(id, location, numCars, price);
    }

    /* Add rooms to a location.
     * This should look a lot like addFlight, only keyed on a string location
     * instead of a flight number.
     */
    public boolean addRooms(int id, String location, int numRooms, int price)
            throws RemoteException {

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);
        methodArguments.put("numRooms", Integer.toString(numRooms));
        methodArguments.put("price", Integer.toString(price));

        MethodCall methodCall = new MethodCall("addRooms", methodArguments);
        MethodResponse methodResponse;

        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Room");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();

//        this.roomManager.addRooms(id, location, numRooms, price);
    }

    /* new customer just returns a unique customer identifier */
    public int newCustomer(int id)
            throws RemoteException
    {

        int cid = Integer.parseInt( String.valueOf(id) +
                String.valueOf(Calendar.getInstance().get(Calendar.MILLISECOND)) +
                String.valueOf( Math.round( Math.random() * 100 + 1 )));

        if (this.newCustomer(id, cid)) {
            System.out.println("Successfully created customer with CID: " + cid);
            return cid;
        } else {
            return -1;
        }

    }

    /* new customer with providing id */
    public boolean newCustomer(int id, int customerID )
            throws RemoteException
    {
        Trace.info("INFO: Middleware::newCustomer(" + id + ", " + customerID + ") called" );

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customerID", Integer.toString(customerID));

        MethodCall methodCall = new MethodCall("newCustomer", methodArguments);
        MethodResponse methodResponse;

        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "All");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();

    }

    /**
     *   Delete the entire flight.
     *   deleteflight implies whole deletion of the flight.
     *   all seats, all reservations.  If there is a reservation on the flight,
     *   then the flight cannot be deleted
     *
     * @return success.
     */
    public boolean deleteFlight(int id, int flightNum)
            throws RemoteException {

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("flightNum", Integer.toString(flightNum));

        MethodCall methodCall = new MethodCall("deleteFlight", methodArguments);
        MethodResponse methodResponse;

        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Flight");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();

//        return this.flightManager.deleteFlight(id, flightNum);
    }

    /* Delete all Cars from a location.
     * It may not succeed if there are reservations for this location
     *
     * @return success
     */
    public boolean deleteCars(int id, String location)
            throws RemoteException {

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("deleteCars", methodArguments);
        MethodResponse methodResponse;

        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall,"Car");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();
//        return this.carManager.deleteCars(id, location);
    }

    /* Delete all Rooms from a location.
     * It may not succeed if there are reservations for this location.
     *
     * @return success
     */
    public boolean deleteRooms(int id, String location)
            throws RemoteException {

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("deleteRooms", methodArguments);
        MethodResponse methodResponse;

        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Room");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();

    }

    /* deleteCustomer removes the customer and associated reservations */
    public boolean deleteCustomer(int id,int customer) throws RemoteException
    {
        Trace.info("Middleware::deleteCustomer(" + id + ", " + customer + ") called" );

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customer));

        MethodCall methodCall = new MethodCall("deleteCustomer", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "All");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();

    }

    /* queryFlight returns the number of empty seats. */
    public int queryFlight(int id, int flightNumber)
            throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("flightNum", Integer.toString(flightNumber));

        MethodCall methodCall = new MethodCall("queryFlight", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Flight");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return -1; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getIntResult();
    }


    /* return the number of cars available at a location */
    public int queryCars(int id, String location)
            throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("queryCars", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Car");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return -1; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getIntResult();
    }


    /* return the number of rooms available at a location */
    public int queryRooms(int id, String location)
            throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("queryRooms", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Room");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return -1; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getIntResult();
    }

    /* return a bill */
    public String queryCustomerInfo(int id, int customerID)
            throws RemoteException
    {

        String fullBill = "";

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customerID));

        MethodCall methodCall = new MethodCall("queryCustomerInfo", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Car");
            fullBill += "Cars: \n" + methodResponse.getStringResult() + "\n";

            methodResponse = this.transactionManager.addOperation(id, methodCall, "Room");
            fullBill += "Rooms: \n" + methodResponse.getStringResult() + "\n";

            methodResponse = this.transactionManager.addOperation(id, methodCall, "Flight");
            fullBill += "Flights: \n" + methodResponse.getStringResult() + "\n";
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return "error"; // If they provided an incorrect transactionId, return false.
        }

        System.out.println( fullBill );
        return fullBill;
    }

    public RMHashtable getCustomerReservations(int id, int customerID)
            throws RemoteException
    {
        return null;
    }

    /* queryFlightPrice returns the price of a seat on this flight. */
    public int queryFlightPrice(int id, int flightNumber)
            throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("flightNum", Integer.toString(flightNumber));

        MethodCall methodCall = new MethodCall("queryFlightPrice", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Flight");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return -1; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getIntResult();
    }

    /* return the price of a car at a location */
    public int queryCarsPrice(int id, String location)
            throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("queryCarsPrice", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Car");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return -1; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getIntResult();
    }

    /* return the price of a room at a location */
    public int queryRoomsPrice(int id, String location)
            throws RemoteException {
        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("queryRoomsPrice", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Room");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return -1; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getIntResult();
    }
    //TODO:

    /* Reserve a seat on this flight*/
    public boolean reserveFlight(int id, int customer, int flightNumber)
            throws RemoteException {

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customer));
        methodArguments.put("flightNumber", Integer.toString(flightNumber));

        MethodCall methodCall = new MethodCall("reserveFlight", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Flight");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();

    }
    //TODO:

    /* reserve a car at this location */
    public boolean reserveCar(int id, int customer, String location)
            throws RemoteException {

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customer));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("reserveCar", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Car");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();

    }
    //TODO:

    /* reserve a room certain at this location */
    public boolean reserveRoom(int id, int customer, String location)
            throws RemoteException {

        HashMap<String, String> methodArguments = new HashMap<String, String>();

        methodArguments.put("id", Integer.toString(id));
        methodArguments.put("customer", Integer.toString(customer));
        methodArguments.put("location", location);

        MethodCall methodCall = new MethodCall("reserveRoom", methodArguments);
        MethodResponse methodResponse;
        try
        {
            methodResponse = this.transactionManager.addOperation(id, methodCall, "Room");
        }
        catch (InvalidTransactionException e)
        {
            e.printStackTrace();
            return false; // If they provided an incorrect transactionId, return false.
        }

        return methodResponse.getBoolResult();


    }

    /* reserve an itinerary */
    public boolean itinerary(int id,int customer,Vector flightNumbers,String location, boolean car, boolean room)
            throws RemoteException {

        Iterator<String> i = flightNumbers.iterator();
        int flightNumber;
        while (i.hasNext()) {
            flightNumber = Integer.parseInt(i.next());
            this.reserveFlight(id, customer, flightNumber);
        }
        if (car) {
            this.reserveCar(id, customer, location);
        }
        if (room) {
            this.reserveRoom(id, customer, location);
        }

        return true;

    }

    @Override
    public int start() throws RemoteException
    {
        System.out.println("at middleware");
        int tid = this.transactionManager.start();
        return tid;
    }

    @Override
    public boolean commit(int transactionId) throws RemoteException, TransactionAbortedException, InvalidTransactionException
    {
        boolean result = false;
        synchronized(this){
            result = this.transactionManager.commit(transactionId);
        };
        return result;
    }

    @Override
    public void abort(int transactionId) throws RemoteException, InvalidTransactionException
    {
        this.transactionManager.abort(transactionId);
    }


    //TODO: Implement Shutdown
    @Override
    public boolean shutdown() throws RemoteException {
        return false;
    }


}

