package ResImpl;

import java.io.*;

public class SerializationHelper
{

    public static byte[] serialize(Object obj) throws IOException
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objOut =  new ObjectOutputStream(byteArrayOutputStream);

        try
        {
            objOut.writeObject(obj);
            objOut.flush();
            return byteArrayOutputStream.toByteArray();
        }
        catch(Exception e)
        {
            System.out.println("error during serialization");
            e.printStackTrace();
        }
        finally
        {
            byteArrayOutputStream.close();
            objOut.close();
        }
        return null;
    }

    public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException
    {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        ObjectInputStream objInput = new ObjectInputStream(byteArrayInputStream);

        try
        {
            return objInput.readObject();
        }
        catch (Exception e)
        {
            System.out.println("error during deserialization");
            e.printStackTrace();
        }
        finally {

            byteArrayInputStream.close();
            objInput.close();
        }
        return null;
    }

}
