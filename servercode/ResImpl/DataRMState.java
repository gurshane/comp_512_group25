package ResImpl;

import ResInterface.MethodCall;

import javax.xml.crypto.Data;
import java.util.HashMap;
import java.util.List;

public class DataRMState extends ClusterState
{
    protected HashMap<Integer, RMHashtable> m_transactionCaches;
    protected RMHashtable m_itemHT;

    protected HashMap<Integer, HashMap<String, RMItem>> m_transactionCache;
    protected HashMap<Integer, List<MethodCall>> m_transactionMethodCalls;
    protected HashMap<Integer, Boolean> m_abortStatus;
    protected LockManager m_LockManager;


    public DataRMState(HashMap<Integer, RMHashtable> m_transactionCaches, RMHashtable m_itemHT, HashMap<Integer, HashMap<String, RMItem>> m_transactionCache, HashMap<Integer, List<MethodCall>> m_transactionMethodCalls, HashMap<Integer, Boolean> m_abortStatus, LockManager m_LockManager) {
        this.m_transactionCaches = new HashMap<>(m_transactionCaches);
        this.m_itemHT = new RMHashtable(m_itemHT);
        this.m_transactionCache = new HashMap<>(m_transactionCache);
        this.m_transactionMethodCalls = new HashMap<>(m_transactionMethodCalls);
        this.m_abortStatus = new HashMap<>(m_abortStatus);
        this.m_LockManager = m_LockManager;
    }

    public DataRMState(){}


}
